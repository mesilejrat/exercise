package com.sdacademy.firstLecture;

import java.util.Scanner;

/*
* Write an application calculating BMI (Body Mass Index) and checking if itЀs optimal or not.
Your application should read two variables: weight (in kilograms, type float) and height
(in centimeters, type int). BMI should be calculated given following formula:
The optimal BMI range is from 18.5 to 24.9, smaller or larger values are non-optimal
values. Your program should write "BMI optimal" or "BMI not optimal", according to the
assumptions above.

* */
public class SecondExercise {

    public static int SHUMEZUESI = 10000;
    public static double MINUMUMI_BMI = 18.5;
    public static double MAKSIMUMI_BMI = 24.9;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Shkruani peshen tuaj:");
        float weight = input.nextFloat();

        System.out.println("Shkruani gjatesine tuaj ne CM: ");
        int height = input.nextInt();

        double bmi = gjejBMI(height,weight);

        if(bmi >= MINUMUMI_BMI && bmi <= MAKSIMUMI_BMI){
            System.out.println("BMI optimal");
        } else {
            System.out.println("BMI not optimal");
        }


    }

    private static double gjejBMI(int height, float weight) {
        return (weight/Math.pow(height,2)) * SHUMEZUESI;
    }
}
