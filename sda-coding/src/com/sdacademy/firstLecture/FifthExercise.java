package com.sdacademy.firstLecture;

/*Write an application that takes a positive number from the user (type int) and prints all
        prime numbers greater than 1 and less than the given number.*/


import java.util.Scanner;

public class FifthExercise {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Shkruani nje numer te plote");
        int numri = input.nextInt();


        for (int count = 2; count <= numri; count++){

           if (isPrime(count)) {
               System.out.println("Numer prime eshte : " + count);
           }

        }


    }

    private static boolean isPrime(int count){

        for(int i = 2; i< count;i++){

            if (count % i == 0){
                return false;
            }

        }
        return true;
    }
}
