package com.sdacademy.firstLecture;

import java.util.Scanner;

public class Ushtrimi7 {

    //me rekursion
    static int fibonacci(int numri){
        if(numri<=1)
            return numri;
        else
            return fibonacci(numri-1) + fibonacci(numri-2);
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Shkruani nje numer pozitiv");
        int numri = input.nextInt();
        int fibonaciNumber = fibonacci(numri);
        System.out.println("Numri eshte :" + fibonaciNumber);

        int n1 = 0;
        int n2 = 1;
        int n3 =0;


        if (numri <= 1){
            System.out.println(1);
        } else{
            for (int i = 1 ; i< numri;i++){
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
            }
        }

        System.out.println(n3);
    }
}
