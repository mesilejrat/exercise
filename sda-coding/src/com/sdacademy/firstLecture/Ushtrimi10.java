package com.sdacademy.firstLecture;

import java.util.Scanner;

public class Ushtrimi10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Fut numrin e rreshtave");
        int rreshta = scan.nextInt();
        System.out.println("Fut numrin e V-ve");
        int kulme = scan.nextInt();

        for (int i = 0; i < rreshta; i++) {
            for (int n = 1; n <= kulme; n++) {
                printoHipotenuzenMajtas(rreshta,i);
                printoHipotenuzenDjathtas(rreshta,i);
            }

            System.out.println();
        }

    }

    public static void printoHipotenuzenMajtas(int rreshta, int i){
        for (int j = 0; j < rreshta; j++) {
            if (j == i) {
                System.out.print(" *");
            } else {
                System.out.print("  ");
            }
        }
    }

    public static void printoHipotenuzenDjathtas(int rreshta, int i){
        for (int k = rreshta - 1; k > 0; k--) {
            if (k == i) {
                System.out.print(" *");
            } else {
                System.out.print("  ");
            }
        }
    }
}
