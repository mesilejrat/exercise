package com.sdacademy.firstLecture;

/*
Write an application that will read diameter of a circle (variable of type float) and
calculate perimeter of given circle.
Firstly, assume π = 3.14. Later, use value of π from built-in Math class.
*/

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

public class FirstExercise {

    public static void main(String[] args) {
        //fillimisht duhet qe te perdor klasen scanner per arsye sepse une duhe te lexoj nga perdoruesi nje numer
        Scanner input = new Scanner(System.in);
        float diametri ;
        System.out.println("Jepni diametrin e rrethit");
        diametri = input.nextFloat();
        float rrezja = diametri/2;

        printoPerimetrinERrethit(diametri);
        printoSiperfaqenERrethit(rrezja);
        printoVelliminESferes(rrezja);

    }

    private static void printoVelliminESferes(float rrezja) {
        //Vellimi i rrethit: 4/3PIr3
        double vellimi = 4.0/3.0*Math.PI*Math.pow(rrezja,3);
        System.out.println("Vellimi i sferes eshte : " + vellimi);
    }

    private static void printoSiperfaqenERrethit(float rrezja) {
        double siperfaqja = Math.PI * Math.pow(rrezja,2);
        System.out.println("Siperfaqja e rrethit eshte : " + siperfaqja);
    }

    //P = Diameter*PI
    private static void printoPerimetrinERrethit(float diametri) {
        double perimetri = diametri*Math.PI;
        System.out.println("Perimetri i rrethit eshte : " + perimetri );
    }
}
