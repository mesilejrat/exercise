package com.sdacademy.firstLecture;

import java.util.Scanner;

/*Write an application that implements a simple calculator. The application should:
        a. read first number (type float)
        b. read one of following symbols: + - / *
        c. read second number (type float)
        d. return a result of given mathematical operation
        If the user provides a symbol other than supported, the application should print "Invalid
        symbol". If the entered action cannot be implemented (i.e. it is inconsistent with the
        principles of mathematics), the application should print "Cannot calculate".*/
public class UShtrimi9 {
    static String WELCOME_KIT = "Ne kete applikacion do vendosni 2 numra dhe nje nga simbolet +-/*";
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println(WELCOME_KIT);
        float firstNumber = input.nextFloat();
        System.out.println("Fusni nje nga simbolet");
        String simbols = input.next();
        System.out.println("Fusni numrin e dyte");
        float secondNumber = input.nextFloat();


        switch (simbols) {
            case "+":
                System.out.println(bejMbledhjen(firstNumber,secondNumber));
                break;
            case "*":
                System.out.println(bejShumezimin(firstNumber, secondNumber));
                break;
            case "/":
                bejPjestimin(firstNumber, secondNumber);
                break;
            case "-":
                System.out.println(bejZbritjen(firstNumber, secondNumber));
                break;
            default:
                System.out.println("Invalid symbol");
        }

    }

    public static float bejMbledhjen(float numer1, float numer2){
        return numer1+numer2;
    }

    public static float bejZbritjen(float numer1, float numer2){
        return numer1-numer2;
    }

    public static float bejShumezimin(float numer1, float numer2){
        return numer1*numer2;
    }

    public static void bejPjestimin(float numer1, float numer2){

       String str = "Cannot calculate";

       if(numer2 != 0){
            System.out.println(numer1/numer2);

        } else{
           System.out.println(str);
       }


    }
}
