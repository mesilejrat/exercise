package com.sdacademy.firstLecture;
/*
Write a program for solving a quadratic equation. The program should take three integers
        (coefficients of the quadratic equation a, b, c) and calculate the roots of the
        equation
        If delta ∆ comes out negative, print "Delta negative" and exit the program.
        Formulas youЀll need
*/

import java.util.Scanner;

public class ThirdExercise {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Jepni koeficentin e pare A: ");
        int a = input.nextInt();
        System.out.println("Jepni koeficentin e dyte B: ");
        int b = input.nextInt();
        System.out.println("Jepni koeficentin e trete C: ");
        int c = input.nextInt();
        //b2 -4ac;
        double delta =Math.pow(b,2) - (4*a*c);
        double rrenja_1 = 0, rrenja_2 =0;
        if (delta >= 0){
            rrenja_1 = (-b - Math.sqrt(delta))/2*a;
            rrenja_2 = (-b + Math.sqrt(delta))/2*a;

            System.out.println("Rrenja e pare eshte :" + rrenja_1);
            System.out.println("Rrenja e dyte eshte :" + rrenja_2);
        } else{
            System.out.println("Delta negative");
        }

    }
}
