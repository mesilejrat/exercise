package com.sdacademy.firstLecture;

import java.util.Scanner;

/*
Write an application that takes a positive number from the user (type int) and writes all
        numbers from 1 to the given number, each on the next line, with the following changes:
        ● in place of numbers divisible by 3, instead of a number the program should print "Fizz"
        ● in place of numbers divisible by 7, instead of a number the program should write
        "Buzz"
        ● if the number is divisible by both 3 and 7, the program should print "Fizz buzz"
*/
public class FourthExercise {

    public static String FIRST_WORD = "Fizz";
    public static String SECOND_WORD = "Buzz";
    public static String THIRD_WORD = "Fizz Buzz";

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Jepni nje numer te plote pozitiv");

        int numri = input.nextInt();

        //
        for (int i =1 ;i<=numri;i++){
            printoFjalenEDuhur(i);
            printoShumeMeTeVogelSe100(i,numri);
            printoNumratCiftDheTek(i);

        }

    }

    private static void printoFjalenEDuhur(int i) {
        if (i % 3 == 0 && i %7 == 0){
            System.out.println(THIRD_WORD);
        } else if (i%7 == 0){
            System.out.println(SECOND_WORD);
        } else if(i % 3 == 0 ){
            System.out.println(FIRST_WORD);
        } else{
            System.out.println(i);
        }
    }

    private static void printoShumeMeTeVogelSe100(int i, int numri){
        if (i + numri < 100){
            System.out.println(i);
        }

    }
    private static void printoNumratCiftDheTek(int i){
        if (i % 2 == 0){
            System.out.println(i + "Numri eshte Cift");
        } else {
            System.out.println(i + "Numri eshte Tek");
        }
    }
}
